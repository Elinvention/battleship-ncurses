#include <stdlib.h>
#include <time.h>
#include <ncurses.h>
#include <stdbool.h>
#include <unistd.h>

#define DIM 10		//DIMENSIONE X E Y
#define PORTAAEREI 5			//DIMENSIONE DELLE NAVI
#define NAVEDAGUERRA 4		//* 
#define SOTTOMARINO 3	//* 
#define CACCIATORPEDINIERE 3		//*
#define PATTUGLIATORE 2		//DIMENSIONE DELLE NAVI
#define PC true
#define PL false

static const char *nomi_navi[] = {"LA PORTAEREI", "LA NAVE DA GUERRA", "IL SOTTOMARINO", "IL CACCIATORPEDINIERE", "IL PATTUGLIATORE"};
static int somma_navipc, somma_navipl;
static const char navic[] = {'A', 'G', 'S', 'C', 'P'};

void creamare(char b[DIM][DIM])	//CREA IL MARE
{
	for(int i=0; i < DIM; i++)
		for(int j=0; j < DIM; j++)
			b[j][i] = '-';
}

int casuale(int a){	//NUMERI RANDOM (MAX NUMERO DA GENERARE)-1
	return rand() % a;
}

//EVITA GLI INCIDENTI NAVALI
bool occupato(char mare[DIM][DIM], int x, int y, bool dir, int dim)
{
	for(int i = 0; i < dim; i++)
	{
		if(dir)
		{
			if(mare[x][y-i] != '-')
				return true;
		}
		else
		{
			if(mare[x+i][y] != '-')
				return true;
		}
	}
	return false;
}

int naveint (char nave)
{
	for (int i = 0; i < 5; i++)
		if (navic[i] == nave) return i;
	return -1;
}

void posiziona(char mare[DIM][DIM], int length, char nave)
{
	int x, y, dir;
	bool posizione_libera = false;
	
	do {
		x = casuale(DIM);
		y = casuale(DIM);
		dir = casuale(2);
		
		if (dir == 0)
		{
			if ((y - length) >= 0 && !occupato(mare, x, y, true, length))
			{
				posizione_libera = true;
				for (int i = 0; i < length; i++)
					mare[x][y-i] = nave;
			}
		}
		else
		{
			if ((x + length) < DIM && !occupato(mare, x, y, false, length))
			{
				posizione_libera = true;
				for(int i = 0; i < length; i++)
					mare[x+i][y] = nave;
			}
		}
	} while(!posizione_libera);
}

void legenda(int col)
{
	mvprintw(0, col/2-15, "*********/ LEGENDA \\*********");
	mvprintw(1, col/2-15, "* A=PORTAAEREI (5)          *");
	mvprintw(2, col/2-15, "* G=NAVEGUERRA (4)          *");
	mvprintw(3, col/2-15, "* S=SOTTOMARINO (3)         *"); 
	mvprintw(4, col/2-15, "* C=CACCIATORPEDINIERE (3)  *");
	mvprintw(5, col/2-15, "* P=PATTUGLIATORE (2)       *");
	mvprintw(6, col/2-15, "*****************************");
}

int somma(int a[5])
{
	int s = 0;
	for(int i = 0; i < 5; i++)
		s += a[i];
	return s;
}

void mostra(WINDOW *local_win, char b[DIM][DIM], bool type)
{
	wbkgd(local_win, COLOR_PAIR(1));	//sfondo blu
	for(int i=0; i < DIM; i++){		//CICLA PER LE Y
		mvwprintw(local_win, 1 + i, 0, "%d", i + 1); 	//NUMERI A SINISTRA
		mvwaddch(local_win, 0, 3 + i * 3, (char)('A' + i));	//LETTERE IN CIMA

		for(int j=0; j < DIM; j++){	//CICLA PER LE X
			if (type == PC)
				if(b[j][i] == '-' || b[j][i] == 'X' || b[j][i] == 'O')
					mvwaddch(local_win, i + 1, 3 + j * 3, b[j][i]);	//MOSTRA LA MATRICE
				else
					mvwaddch(local_win, i + 1, 3 + j * 3, '-');
			else
				mvwaddch(local_win, i + 1, 3 + j * 3, b[j][i]);
		}
	}
	wrefresh(local_win);
}

int coordx2cursorx(int x)
{
	return x * 3 + 3;
}
int coordy2cursory(int y)
{
	return y + 1;
}
int cursorx2coordx(int x)
{
	return (x - 3) / 3;
}
int cursory2coordy(int y)
{
	return y - 1;
}

void colpito(WINDOW *w, int coordx, int coordy)
{
	wattron(w, COLOR_PAIR(2));
	mvwprintw(w, coordy2cursory(coordy), coordx2cursorx(coordx) - 1, " X ");
	wattroff(w, COLOR_PAIR(2));
	wrefresh(w);
}

void mancato(WINDOW *w, int coordx, int coordy)
{
	wattron(w, COLOR_PAIR(3));
	mvwaddch(w, coordy2cursory(coordy), coordx2cursorx(coordx), 'O');
	wattroff(w, COLOR_PAIR(3));
	wrefresh(w);
}

bool checknextstep(char b[DIM][DIM], int y, int x)
{
	return (x < DIM && x >= 0 && y < DIM && y >= 0 && b[x][y] != 'O' && b[x][y] != 'X');
}

void turnopc(WINDOW *w, char mare[DIM][DIM], int navi[5])
{
	static int x = 0, y = 0, memx, memy, dir = 0;//dir=0(CASUALE); dir=1(X++); dir=2(X--); dir=3(Y++); dir=4(Y--);
	bool next;

	do {
		next = false;
		switch(dir)
		{
			case 0:
				x = casuale(DIM);
				y = casuale(DIM);
				next = checknextstep(mare, y, x);
				break;
			case 1:
				if ((next = checknextstep(mare, y, x + 1)))
					x++;
				else
				{
					dir++;
					x = memx;
					y = memy;
				}
				break;
			case 2:
				if ((next = checknextstep(mare, y, x - 1)))
					x--;
				else
				{
					dir++;
					x = memx;
					y = memy;
				}
				break;
			case 3:
				if ((next = checknextstep(mare, y + 1, x)))
					y++;
				else
				{
					dir++;
					x = memx;
					y = memy;
				}
				break;
			case 4:
				if ((next = checknextstep(mare, y - 1, x)))
					y--;
				else
				{
					dir = 0;
					x = memx;
					y = memy;
				}
				break;
		}
	} while (!next);	//non esco di qua finchè non becco una nave o un -

	int nave = naveint(mare[x][y]);
	
	if(mare[x][y] == '-')
	{
		mare[x][y] = 'O';
		mancato(w, x, y);
		x = memx;
		y = memy;
		//mvprintw(22, (DIM + 1) * 3 + 5, "SBIISH!!!");
		//system("paplay WW_Salvatore_Sploosh.wav&");
	}
	else if (mare[x][y] != 'X' && mare[x][y] != 'O')
	{
		navi[nave]--;
		mare[x][y] = 'X';
		colpito(w, x, y);
		if (dir == 0)
		{
			dir = 1;
			memx = x;
			memy = y;
		}
		if (navi[nave] == 0)
		{
			char espeak[100];
			sprintf(espeak, "paplay WW_Salvatore_Wuhahahaha.wav && espeak -v it 'IL NEMICO HA AFFONDATO %s'&", nomi_navi[nave]);
			system(espeak);
		} else {
			system("paplay WW_Salvatore_Wuhahahaha.wav&");
		}
	}
}

void input(WINDOW *w, int *coordx, int *coordy)
{
	static int cursorx = 3, cursory = 1;
	int ch;
	do {
		move(8, 0);
		clrtoeol();
		mvprintw(8, 0, "MARE GIOCATORE. %d OBIETTIVI", somma_navipl);
		mvprintw(8, (DIM + 1) * 3 + 5, "MARE NEMICO. %d OBIETTIVI", somma_navipc);
		move(22, 0);
		clrtobot();
		mvprintw(LINES - 1, (DIM + 1) * 3, "Coordinate: %c%d.", (char)(*coordx + 'A'), *coordy + 1);
		refresh();
		wmove(w, cursory, cursorx);
		ch = wgetch(w);
		switch(ch)
		{	case KEY_UP:
				if(cursory - 1 > 0)
					cursory--;
				break;
			case KEY_DOWN:
				if(cursory + 1 <= DIM)
					cursory++;
				break;
			case KEY_LEFT:
				if (cursorx - 3 >= 3)
					cursorx -= 3;
				break;
			case KEY_RIGHT:
				if (cursorx + 3 <= 3 * DIM)
					cursorx += 3;
				break;
			case 'q':
			case 'Q':
				endwin();
				exit(EXIT_SUCCESS);
				break;
			case '\n':
				break;
			default:
				mvprintw(22, 0, "Hai premuto %3d [%d %d %d %d]!", ch, KEY_UP, KEY_DOWN, KEY_LEFT, KEY_LEFT, KEY_RIGHT);
				break;
		}
		*coordx = cursorx2coordx(cursorx);
		*coordy = cursory2coordy(cursory);
	} while(ch != '\n');
	

}

void turnopl(WINDOW *w, char mare[DIM][DIM], int navipc[5])
{
	int coordx, coordy;
	
	input(w, &coordx, &coordy);	//leggo le coordinate
	
	if(mare[coordx][coordy] == '-')	//mancato
	{
		mare[coordx][coordy] = 'O';
		mancato(w, coordx, coordy);
		//mvprintw(22, (DIM + 1) * 3 + 5, "SBIISH!!!");
		system("paplay WW_Salvatore_Sploosh.wav&");
	}
	else if (mare[coordx][coordy] != 'X' && mare[coordx][coordy] != 'O')	//colpito
	{
		int nave = naveint(mare[coordx][coordy]);
		navipc[nave]--;
		mare[coordx][coordy] = 'X';

		colpito(w, coordx, coordy);

		mvprintw(22, (DIM + 1) * 3 + 5, "CABOOM!!!");
		if (navipc[nave] == 0)
		{
			char espeak[100];
			sprintf(espeak, "paplay WW_Salvatore_Kerboom.wav && espeak -v it 'HAI AFFONDATO %s'&", nomi_navi[nave]);
			mvprintw(22, 0, "HAI AFFONDATO %s", nomi_navi[nave]);
			system(espeak);
		} else {
			system("paplay WW_Salvatore_Kerboom.wav&");
		}
	}
}

int main(){
	char marepc[DIM][DIM], marepl[DIM][DIM];
	int row, col;	//dimensioni del terminale
	int navipc[] = {PORTAAEREI, NAVEDAGUERRA, SOTTOMARINO, CACCIATORPEDINIERE, PATTUGLIATORE}; //STATO DELLE NAVI DEL PC
	int navipl[] = {PORTAAEREI, NAVEDAGUERRA, SOTTOMARINO, CACCIATORPEDINIERE, PATTUGLIATORE}; //STATO DELLE NAVI DEL PLAYER

	WINDOW *w_pl, *w_pc;	//finestre nel quale visualizzare le due mappe
	
	initscr();	//avvia ncurses
	getmaxyx(stdscr,row,col);	//numero di righe e colonne del terminale
	cbreak();	//non va a capo con invio
	noecho();	//non scrivere i tasti che premo
	start_color();

	init_pair(1, COLOR_WHITE, COLOR_BLUE);
	init_pair(2, COLOR_WHITE, COLOR_RED);
	init_pair(3, COLOR_WHITE, COLOR_BLACK);
	
	srand((unsigned)time(NULL));
	
	creamare(marepc);
	creamare(marepl);
	
	for(int i=0; i < 5; i++){
		posiziona(marepl, navipl[i], navic[i]);
		posiziona(marepc, navipc[i], navic[i]);
	}
	
	legenda(col);	//visualizza legenda
	refresh();
	
	w_pl = newwin(DIM + 1, (DIM + 1) * 3, 10, 0);
	w_pc = newwin(DIM + 1, (DIM + 1) * 3, 10, (DIM + 1) * 3 + 5);
	
	mostra(w_pc, marepc, PC);
	mostra(w_pl, marepl, PL);
	
	system("paplay WW_Salvatore_Siren.wav&");
	
	keypad(w_pc, TRUE);
	
	do {
		turnopl(w_pc, marepc, navipc);
		turnopc(w_pl, marepl, navipl);
		
		refresh();
		wrefresh(w_pc);
		wrefresh(w_pl);
		
		somma_navipc = somma(navipc);
		somma_navipl = somma(navipl);
	} while (somma_navipc > 0 && somma_navipl > 0);
	
	
	delwin(w_pc);
	delwin(w_pl);
	clear();
	
	if(somma_navipc == 0)
		mvprintw(row / 2, col / 2 - 4, "VINTO!!!");
	else
		mvprintw(row / 2, col / 2 - 4, "PERSO!!!");

	mvprintw(row / 2 + 2, col / 2 - 5, "premi invio");
	refresh();
	sleep(1);
	system("paplay WW_Salvatore_HoorayYay.wav&");
	
	while(getch()!= '\n');
	
	endwin();
	return 0;
}
